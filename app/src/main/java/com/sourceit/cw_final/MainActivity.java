package com.sourceit.cw_final;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      contentResolve();
    }

    private void contentResolve() {

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, new String[]{ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME, ContactsContract.Contacts.HAS_PHONE_NUMBER}, null, null, ContactsContract.Contacts.DISPLAY_NAME + "ASC");

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {

                String id = cur.getString(0);
                String name = cur.getString(1);
                if (cur.getInt(2) > 0) {
                    List<String> list = getNumbers(id);
                    Log.i("msg",name + " " + list );

                }
            }
        }


    }

    private List<String> getNumbers(String id) {
        List<String> list = new ArrayList<>();
        Cursor pCur = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                new String[]{id},
                null);
        while (pCur.moveToNext()){
            String phoneNo = pCur.getString(0);
            list.add(phoneNo);
            Log.i("userName",phoneNo);

        }
        pCur.close();
        return list;


    }


}
